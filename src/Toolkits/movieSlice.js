import { createSlice } from "@reduxjs/toolkit"


const movieSlice = createSlice({
    name: "movie",
    initialState: {
        movieDetailSchedule: [],
        spinnerSlice: false,
    },
    reducers: {
        setDetailSchedule: (state, action) => {
            state.movieDetailSchedule = action.payload;
        },
        setLoadingOn: (state) => {
            state.spinnerSlice = true;
        },
        setLoadingOff: (state) => {
            state.spinnerSlice = false;
        },
    },
});

export const { setDetailSchedule, setLoadingOn, setLoadingOff } = movieSlice.actions;

export default movieSlice.reducer;
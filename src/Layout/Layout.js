import React from 'react'
import Header from '../Components/Header/Header'
import Footer from '../Components/Footer/Footer';

const Layout = ({ Component }) => {
    return (
        <div className='min-h-screen flex flex-col'>
            <Header />
            <div className="flex-gow">
                <Component />
            </div>
            <Footer />
        </div>
    )
}

export default Layout;

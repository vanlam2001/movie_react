import HomePage from "../HomePage/HomePage";
import Layout from "../Layout/Layout";
import DetailPage from "../Page/DetailPage/DetailPage";
import LoginPage from "../Page/LoginPage/LoginPage";
import RegisterPage from "../Page/RegisterPage/RegisterPage";

export const userRoute = [
    {
        path: '/',
        component: <Layout Component={HomePage} />
    },
    {
        path: '/login',
        component: <Layout Component={LoginPage} />
    },

    {
        path: '/register',
        component: <Layout Component={RegisterPage} />
    },
    {
        path: '/detail/:id',
        component: <Layout Component={DetailPage} />
    }
]
import { message } from "antd";
import validator, { isLength } from 'validator';

export const checkUserName = (userName) => {
    let checkUserName = false;
    if (validator.matches(userName, /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/)) {
        checkUserName = true;
    } else {
        message.error('Tài khoản ít nhất 6 ký tự bao gồm cả chữ và số')
    }

    return checkUserName;
}

export const checkPassword = (passwords) => {
    let checkPassword = false;
    const isValidPassword = isLength(passwords.trim(), { min: 4 });

    if (isValidPassword) {
        checkPassword = true;
    } else {
        message.error("Mật khẩu ít nhất 4 ký tự trở lên!");
    }

    return checkPassword;
}

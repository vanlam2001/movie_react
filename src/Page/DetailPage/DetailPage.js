import React, { useEffect } from 'react'
import DetailMovie from './DetailMovie';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setDetailSchedule, setLoadingOff, setLoadingOn } from '../../Toolkits/movieSlice';
import { movieServ } from '../../services/movieService';
import TabsDetailSchedule from './TabsDetailSchedule';

const DetailPage = () => {
    const params = useParams();
    let dispatch = useDispatch();
    useEffect(() => {
        dispatch(setLoadingOn);
        movieServ.getMovieDetailSchedule(params.id)
            .then((res) => {
                dispatch(setLoadingOff);
                dispatch({
                    type: setDetailSchedule,
                    payload: res.data.content,
                })
            })
            .catch((err) => {
                dispatch(setLoadingOff);
                console.log(err);
            })
    }, [])
    return (
        <div className='container mx-auto max-w-[960px] px-6'>
            <div className="py-10">
                <DetailMovie />
                <TabsDetailSchedule />
            </div>
        </div>
    )
}

export default DetailPage;

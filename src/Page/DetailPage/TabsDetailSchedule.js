import { Tabs } from 'antd';
import React from 'react'
import { useSelector } from 'react-redux';
import ItemsTabsDetailSchedule from './ItemsTabsDetailSchedule';

const TabsDetailSchedule = () => {
    let movie = useSelector((state) => state.movieSlice.movieDetailSchedule)
    const onChange = (key) => {
        console.log(key);
    }
    let renderShedule = () => {
        return movie?.heThongRapChieu?.map((heThongRap) => {
            return {
                key: heThongRap.maHeThong,
                label: (
                    <div>
                        <img src={heThongRap.logo} style={{ width: 80 }} alt="" />
                        <span>{heThongRap.tenHeThongRap}</span>
                    </div>
                ),
                children: (
                    <div>
                        {heThongRap.cumRapChieu.map((cumRap) => {
                            return <ItemsTabsDetailSchedule key={cumRap.maCumRap} cumRap={cumRap} />
                        })}
                    </div>
                )
            }
        })
    }
    return (
        <div className='border-2 border-gray-200 mt-10'>
            <Tabs style={{ height: 700 }}
                tabPosition='left'
                defaultActiveKey="1"
                items={renderShedule()}
                onChange={onChange}
            />
        </div>
    )
}

export default TabsDetailSchedule;

import React from 'react'
import UserMenu from './UserMenu';

const Header = () => {
    return (
        <div className='h-20 shadow'>
            <div className="container mx-auto flex justify-between items-center h-full">
                <span className='font-bold text-red-500 text-2xl animate-pulse'>
                    CyberFlix
                </span>
                <UserMenu />
            </div>
        </div>
    )
}

export default Header;

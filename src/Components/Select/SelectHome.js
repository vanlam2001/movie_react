import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { Select } from 'antd';
import { movieServ } from '../../services/movieService';
import moment from 'moment';

const SelectHome = () => {
    let navigate = useNavigate();
    const [listMovie, setListMovie] = useState([]);
    const [movieCinema, setMovieCinema] = useState({});
    const [movieSchedule, setMovieSchedule] = useState([]);
    const [idMovieTime, setidMovieTime] = useState("");

    useEffect(() => {
        movieServ.getMovieList()
            .then((res) => {
                setListMovie(res.data.content)
                console.log(res.data.content)
            })

            .catch((err) => {
                console.log(err)
            })
    }, [])


    const searchBox = [
        {
            label: "Phim",
        },
        {
            label: "Rạp",
        },
        {
            label: "Ngày giờ chiếu",
        },
    ];

    const getOptions = (type) => {
        if (type === "Phim") {
            return listMovie?.map((item) => {
                return {
                    value: item.maPhim,
                    label: item.tenPhim,
                };
            });
        }
        else if (type === "Rạp") {
            let listCumRap = [];
            movieCinema?.heThongRapChieu?.forEach((item) => {
                item.cumRapChieu.forEach((cumRap) => {
                    let newCumRap = {
                        value: JSON.stringify(cumRap),
                        label: cumRap.tenCumRap,
                    };
                    listCumRap.push(newCumRap);
                })
            })
            return listCumRap;
        }

        else if (type === "Ngày giờ chiếu") {
            return movieSchedule?.lichChieuPhim?.map((item) => {
                return {
                    value: item.maLichChieu,
                    label: moment(item.ngayChieuGioChieu).format("hh:mm ~ DD/MM"),
                };
            });
        }
        else {
            return listMovie?.map((item) => {
                return {
                    value: item.maPhim,
                    label: item.tenPhim,
                };
            });
        }
    }

    const handleChange = (id, label) => {
        if (label === "Phim") {
            movieServ.getMovieSchelude(id)
                .then((res) => {
                    setMovieCinema(res.data.content);
                })
                .catch((err) => {
                    console.log(err);
                });
        } else if (label === "Rạp") {
            setMovieSchedule(JSON.parse(id));
        } else if (label === "Ngày giờ chiếu") {
            setidMovieTime(id);
        }
    };

    return (
        <div className='container mx-auto mt-5'>
            <div className='selectMovie flex items-center shadow rounded-md max-w-full w-[960px]  mx-auto px-5 py-2'>
                <div className='listSelect flex grow'>
                    {searchBox.map((item, index) => {
                        return (
                            <div key={index} className="itemSelect grow px-2">
                                <Select
                                    className='w-full'
                                    defaultValue={{
                                        value: item.label,
                                        label: item.label,
                                    }}
                                    placeholder={item.label}
                                    optionFilterProp="children"
                                    onChange={(id) => { handleChange(id, item.label) }}

                                    filterOption={(input, option) =>
                                        (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                                    }
                                    options={getOptions(item.label)}

                                />
                            </div>
                        )
                    })}
                </div>
                <div>
                    <button className='px-6 py-3 ml-4 bg-orange-500 rounded text-white'>Mua Vé Ngay</button>
                </div>
            </div>
        </div>
    )
}

export default SelectHome;
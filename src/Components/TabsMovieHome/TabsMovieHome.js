import { Tabs } from 'antd';
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { movieServ } from '../../services/movieService';
import ItemTabsMovieHome from './ItemTabsMovieHome';
import './tabsMovieHome.css';
const onChange = (key) => {
    console.log(key);
};

const TabsMovieHome = () => {
    const dispatch = useDispatch();
    const [listHeThongRap, setListHeThongRap] = useState([])
    useEffect(() => {
        movieServ.getMovieByTheater()
            .then((res) => {
                setListHeThongRap(res.data.content)
            })
            .catch((err) => {
                console.log(err)
            })
    }, [])

    const renderTheater = () => {
        return listHeThongRap.map(heThongRap => ({
            key: heThongRap.maHeThongRap,
            label: <img style={{ width: 50 }} src={heThongRap.logo} alt="" />,
            children: (
                <Tabs style={{ height: 700 }} tabPosition='left' defaultActiveKey="1" items={heThongRap.lstCumRap.map(cumRap => ({
                    key: cumRap.tenCumRap,
                    label: (
                        <div className='text-left w-60'>
                            <h3 className='truncate text-lg font-bold text-green-500'>{cumRap.tenCumRap}</h3>
                            <p className='truncate text-gray-500'>{cumRap.diaChi}</p>
                            <p className='text-red-500'>[Chi tiết]</p>
                        </div>
                    ),
                    children: (
                        <div className='w-full h-[700px] overflow-x-scroll'>
                            {cumRap.danhSachPhim.map((movie, index) => {
                                return <ItemTabsMovieHome key={index} movie={movie} />
                            })}
                        </div>
                    )
                }))}
                    onChange={onChange}
                />
            )
        }))
    }
    return (
        <div className='container hidden md:block mx-auto mt-10 max-w-full px-2'>
            <div className="w-full mx-auto max-w-[930px] border-2">
                <Tabs style={{ height: 700 }}
                    tabPosition='left'
                    defaultActiveKey='1'
                    items={renderTheater()}
                    onChange={onChange}
                />
            </div>
        </div>
    )
}

export default TabsMovieHome;

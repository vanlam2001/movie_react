export const USER_INFO = 'USER_INFO'

export const localUserServ = {
    get: () => {
        // lấy dữ liệu từ localStorage lên -> đổ vào headerInfo (D)
        let jsonData = localStorage.getItem(USER_INFO);
        // return jsonData ? JSON.parse(jsonData) : ""
        if (jsonData != "" && jsonData != "undifined") {
            return JSON.parse(jsonData) ? JSON.parse(jsonData) : null;
        }
    },

    set: (userInfo) => {
        // sau khi login thành công -> lưu userInfo xuống localStorage (Q)
        let jsonData = userInfo ? JSON.stringify(userInfo) : "";
        localStorage.setItem(USER_INFO, jsonData)
    },

    remove: () => {
        // sau khi người dùng đăng xuất -> xóa userInfo ở localStorage (D)
        localStorage.removeItem(USER_INFO);
    }
}
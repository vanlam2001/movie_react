import { https } from "./config"

export const movieServ = {
    getMovieList: () => {
        return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07");
    },

    getMovieSchelude: (id) => {
        return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`);
    },

    getMovieByTheater: () => {
        return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap");
    },

    getMovieDetailSchedule: (id) => {
        return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`);
    }
}


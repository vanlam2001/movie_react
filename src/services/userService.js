import { https } from "./config"

export const userServ = {
    loginUser: (login) => {
        return https.post('/api/QuanLyNguoiDung/DangNhap', login);
    },

    register: (register) => {
        return https.post('/api/QuanLyNguoiDung/DangKy', register);
    }
};
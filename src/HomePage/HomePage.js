import React from 'react'
// import Banner from '../Components/Banner/Banner';
import SelectHome from '../Components/Select/SelectHome';
import ListMovie from '../Components/ListMovie/ListMovie';
import TabsMovieHome from '../Components/TabsMovieHome/TabsMovieHome';


const HomePage = () => {
    return (
        <div>
            {/* <Banner /> */}
            <SelectHome />
            <ListMovie />
            <TabsMovieHome />
        </div>
    )
}

export default HomePage;
